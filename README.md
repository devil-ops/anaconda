## Synopsis

This code is used to deploy anacond to an Ubuntu host.  It should be run from the host on which you want it deployed using an ansible playbook

## Usage

```
./install.sh
```
